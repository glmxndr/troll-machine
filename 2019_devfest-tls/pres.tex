%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% I, the copyright holder of this work, release this work into the
%% public domain. This applies worldwide. In some countries this may
%% not be legally possible; if so: I grant anyone the right to use
%% this work for any purpose, without any conditions, unless such
%% conditions are required by law.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass{beamer}

\usetheme[faculty=fi]{fibeamer}
\usepackage[utf8]{inputenc}
\usepackage[main=french,english]{babel}        %% typeset as follows:
%%
%%   \begin{otherlanguage}{czech}   ... \end{otherlanguage}
%%   \begin{otherlanguage}{slovak}  ... \end{otherlanguage}
%%
%% These macros specify information about the presentation

\title{Le Troll dans ta Machine} %% that will be typeset on the
\subtitle{Une courte introduction à la pensée de J.-Y. \textsc{Girard}} %% title page.
\author{\texorpdfstring{Guillaume Andrieu \quad \raisebox{-0.7ex}{\includegraphics[scale=0.2]{resources/Logo_MonkeyPatch_blanc.png}}\newline \faTwitter @glmxndr}{Guillaume Andrieu}}
\institute{MonkeyPatch}
\date{\today}

%% These additional packages are used within the document:
\usepackage{ragged2e}  % `\justifying` text
\usepackage{booktabs}  % Tables
\usepackage{tabularx}
\usepackage{tikz}      % Diagrams
\usetikzlibrary{calc, shapes, backgrounds}
\usepackage{amsmath, amssymb}
\usepackage{url}       % `\url`s
\usepackage{listings}  % Code listings
\usepackage{verbatim}
\usepackage{marvosym}
\usepackage{stmaryrd}
\usepackage{fontawesome}
\usepackage{trollface/trollface}

\newcommand{\qta}[2]{%
  {\Huge«}\,{\Large #1}

  \hspace{1cm} --- #2
}
\newcommand{\qtat}[3]{%
  {\Huge«}\,{\Large #1}

  \hspace{1cm} --- #2, \emph{#3}
}
\newcommand{\qtatpy}[5]{%
  {\Huge«}\,{\Large #1}

  \hspace{1cm} --- #2, \emph{#3} #4 (#5)
}

\newcommand{\reflink}[1]{%
  \par\vfill\hfill{\scriptsize\url{#1}}%
}

\frenchspacing
\begin{document}
  \setbeamertemplate{headline}[text line]{%
    \parbox{\linewidth}{
      \includegraphics[scale=0.07]{resources/devfest_full.png}}}
    
  \frame{\maketitle}

  \setbeamertemplate{headline}[text line]{}
  \setbeamertemplate{footline}[text line]{%
  \parbox{\linewidth}{
    \vspace*{-20pt}
    \includegraphics[scale=0.2]{resources/devfest_toulouse_2019_color72.png}
    \faTwitter @glmxndr
    \hfill
    \insertframenumber/\inserttotalframenumber
    }
  }

  \section{Une courte présentation de JYG}

  \begin{frame}
    \frametitle{Mais qui est Jean-Yves Girard?}
    \begin{itemize}
      \item Probablement le logicien le plus important depuis les années 1970.
      \item Travaux les plus connus:
      \begin{itemize}
        \item Système F (typage, polymorphisme)
        \item Logique linéaire ("logique des ressources")
      \end{itemize}
      \pause
      \item Leurs fruits sont \emph{partout} en informatique.
      \pause
      \bigskip
      \item Souvent abrupt. {\fontsize{50}{60}\selectfont\trollfaceB}
            \\ Mais pourquoi?
    \end{itemize}
  \end{frame}

  \section{Déconstruction}

  \subsection{À bas le scientisme!}

  \begin{darkframes}
    
    \begin{frame}
      \frametitle{Déconstruction - Le scientisme}
      \framesubtitle{Rejet des totalitarismes (1)}
      \qta{
        \textbf{scientisme:} (n.m.) Opinion philosophique, de la fin du XIXe s., qui
        affirme que la science nous fait connaître la totalité des choses qui
        existent[\dots]
      }{Dictionnaire Larousse}
      \pause
      \par\vfill
      \begin{center}
      Tout est {\Large objectif}!
      \end{center}
    \end{frame}

    \begin{frame}
      \frametitle{Déconstruction - Le scientisme}
      \framesubtitle{Hilbert -- Bim!}
      \qtatpy{
        “Solutions finales”, c’est le joli terme employé par Hilbert et
        consorts. Choquant, mais ce n’était qu’un poncif scientiste, recyclé par
        le IIIe Reich.
      }{JYG}{Le Point Aveugle}{p.30}{2006}
    \end{frame}

    \subsection{À bas le logicisme!}

    \begin{frame}
      \frametitle{Déconstruction - Le logicisme}
      \framesubtitle{Rejet des totalitarismes (2)}
      \qta {
        \textbf{logicisme} {[\dots]} tous les concepts et théories mathématiques
        sont réductibles à la logique.
      }{Wikipédia, "Logicisme"}
      \par\vfill
      \emph{Credo} logiciste:
      \begin{itemize}
        \item "La vérité vient du réel"
        \pause
        \item "La logique est la mécanique du vrai"
      \end{itemize}
    \end{frame}

    \begin{frame}
      \frametitle{Déconstruction - Le logicisme}
      \framesubtitle{La cohérence?}
      La cohérence d'un système logique, c'est (au choix):
      \begin{itemize}
      \item absence de contradiction
      \item prouvable $\Rightarrow$ vrai
      \item $\forall A\ . (A \Rightarrow \neg\neg A)$
      \end{itemize}
    \end{frame}

    \begin{frame}
      \frametitle{Déconstruction - Le logicisme}
      \framesubtitle{Théorèmes d'incomplétude - Gödel \emph{circa} 1930}
      \qtat{on ne peut pas revisser ses lunettes en les gardant sur le nez}
      {JYG}{Le théorème de Gödel ou une soirée avec M. Homais}
      \par\vfill
      \begin{itemize}
        \item Un système logique un peu évolué ne peut pas prouver sa propre cohérence.
        \item $\Rightarrow$ Ci-gît le logicisme. \emph{R.I.P.} \Cross
      \end{itemize}
      \reflink{http://girard.perso.math.cnrs.fr/godel.pdf}
    \end{frame}

    \begin{frame}
      \frametitle{Déconstruction - Le logicisme}
      \framesubtitle{Russell -- Bam!}
      \qtatpy{
        Le problème, c'est que de la mauvaise philosophie (Russell, pour
        simplifier) a complètement enlevé toute substance à la logique.
      }{JYG}{Qu'est-ce qu'une réponse?}{01:38}{2014}
      \reflink{https://www.youtube.com/watch?v=f7sT0J74pHI}
    \end{frame}

    \begin{frame}
      \frametitle{Déconstruction - Le logicisme}
      \framesubtitle{Problème de l'arrêt - Turing \emph{circa} 1936}
      \qtatpy{Une version simplifiée de l’incomplétude, l’indécidabilité, est
        due à Turing (1936). Les deux résultats répondent négativement à la
        question de répondre à tout.}{JYG}{Titres et travaux}{p.6}{2018}
      \par\vfill
      \begin{itemize}
        \item Aucun programme ne peut prouver l'arrêt de tout programme
        \pause
        \item Tiré directement des travaux de Gödel
        \pause
        \bigskip
        \item \textit{Premiers liens directs entre calculabilité et prouvabilité\,!}
      \end{itemize}
      \par\vfill
      \hfill{\scriptsize\url{http://girard.perso.math.cnrs.fr/titres.pdf}}
    \end{frame}

    \subsection{À bas le «sémanticisme»!}

    \begin{frame}
      \frametitle{Déconstruction - La sémantique}
      \framesubtitle{La sémantique, zombie du réalisme logique}
      La logique ne tient pas debout tout seule\,!\\
      Vite, une béquille\,!
      \par\vfill
      \emph{Credo} sémantique
      \begin{itemize}
        \item "Le langage (formel) ne veut rien dire tout seul"
        \pause
        \item "Le sens du langage vient du réel"
        \pause
        \item "La sémantique est l'intermédiaire entre langage et réel"
      \end{itemize}
    \end{frame}

    \begin{frame}
      \frametitle{Déconstruction - La sémantique}
      \framesubtitle{La sémantique réaliste de Tarski}
      Sémantique de la disjonction
      \par\vfill
      \begin{itemize}
        \item $A$ ou $B$ vrai $\iff$ $A$ vrai méta-ou $B$ vrai
        \pause 
        \item $A$ méta-ou $B$ vrai $\iff$ $A$ vrai méta-méta-ou $B$ vrai
        \pause 
        \item \emph{ad libitum} (= jusqu'à épuisement)
      \end{itemize}
    \end{frame}

    \begin{frame}
      \frametitle{Déconstruction - La sémantique}
      \framesubtitle{Et alors, où est le problème?}
      A-t-on \emph{vraiment} besoin d'expliquer "ou" ?

      \pause
      Lequel? Inclusif? Exclusif?
    \end{frame}

    \begin{frame}
      \frametitle{Déconstruction - La sémantique}
      \framesubtitle{La sémantique réaliste de Tarski (bis)}
      Sémantique de $\clubsuit$ (prononcez «broccoli»)
      \par\vfill
      \begin{itemize}
        \item $A$ $\clubsuit$ $B$ vrai $\iff$ $A$ vrai méta-$\clubsuit$ $B$ vrai
        \pause 
        \item $A$ méta-$\clubsuit$ $B$ vrai $\iff$ $A$ vrai méta-méta-$\clubsuit$ $B$ vrai
        \pause 
        \item {\fontsize{50}{60}\selectfont \trollfaceW}
      \end{itemize}
    \end{frame}

    \begin{frame}
      \frametitle{Déconstruction - La sémantique}
      \framesubtitle{Tarski - Boum!}
      \qtatpy{Ce qui nous ramène au Diafoirus de Molière: l'opium fait dormir de
        par sa \emph{vertu dormitive}.}{JYG}{Le fantôme de la transparence}{p.9}{2010}
      \par\vfill
      \hfill{\scriptsize\url{http://girard.perso.math.cnrs.fr/transparence.pdf}}
    \end{frame}

    \begin{frame}
      \frametitle{Déconstruction - La sémantique}
      \framesubtitle{Ne riez pas, on a fait pire!}
      Interpréteur de JS en JS: 
        \\ \url{https://github.com/jterrace/js.js/}
      \pause
      \vfill
      \begin{columns}[onlytextwidth]
        \column{.5\textwidth}
          Interpréteur de Java en Java: 
          \\ \url{https://kotlinlang.org/}
        \column{.5\textwidth}
        {\fontsize{50}{60}\selectfont \trollfaceW}
      \end{columns}
    \end{frame}
  \end{darkframes}

  \section{Reconstruction}

  \subsection{Les 3 sous-sols}

  \begin{frame}<beamer>
    \frametitle{Et TOC!}
    \tableofcontents[currentsection]
  \end{frame}

  \begin{frame}
    \frametitle{Reconstruction - Les sous-sols}
    \framesubtitle{Factorisation}
    Trois niveaux de profondeur d'interprétation de la logique:
    \par\vfill
    \qtatpy{
      [\dots] la métaphore du \emph{courrier}:\newline
      \textbf{-1}\,:\,le destinataire de la lettre\newline
      \textbf{-2}\,:\,le contenu de la lettre\newline
      \textbf{-3}\,:\,le processus d'écriture, d'acheminement 
                      (prix du timbre, bureau de tri, facteur...)      
    }{JYG}{La logique aujourd'hui...}{p.6}{2007}
    \reflink{https://girard.perso.math.cnrs.fr/roma2004.pdf}
  \end{frame}

  \begin{frame}
    \frametitle{Reconstruction - Les sous-sols}
    \framesubtitle{Trois lectures des sous-sols}
    \begin{tabularx}{\textwidth}{c|X|X|X}
      \textbf{Niveau} & \textbf{Courrier} & \textbf{Logique} & \textbf{Calcul} \\
      \hline
      -1 & Destinataire   & Prouvabilité & Terminaison      \\
      -2 & Contenu        & Preuve       & Valeur           \\
      -3 & Acheminement   & Intéractions & Protocole        \\
    \end{tabularx}
  \end{frame}

  \begin{frame}
    \frametitle{Reconstruction - Niveau -1}
    \framesubtitle{Meh}
    \begin{center}Tout est {\Large Objectif}!\end{center}
  \end{frame}

  \subsection{Système F}

  \begin{frame}
    \frametitle{Reconstruction - Niveau -2}
    \framesubtitle{Le système F}
    \begin{itemize}
      \item Très simple, seulement deux opérateurs de base: 
      \begin{itemize}
        \item Implication $\to$
        \item Quantificateur universel $\forall$
        \pause
        \item Par exemple: $\forall A. A \to (\forall B. (A \to B) \to B)$
      \end{itemize}
      \pause
      \item Les propositions sont des types, \\
            les preuves sont des fonctions!
      \pause
      \item Systèmes de typage polymorphiques
      \begin{itemize}
        \item ML/Haskell --- \textsf{id :: forall a. a -> a}
        \item Java --- \textsf{<T> List<T> append(List<T> ts, T t);}
      \end{itemize}
      \pause
      \item Très (trop?) expressif
    \end{itemize}
  \end{frame}

  \subsection{Logique linéaire}

  \begin{frame}
    \frametitle{Reconstruction - Niveau -3}
    \framesubtitle{La logique linéaire par le menu (idée de Yves Lafont)}
    \setbeamercovered{transparent}
    \begin{enumerate}
      \item[]<1> \begin{center}\textbf{Menu à 20 Francs}\end{center}
      \item[] \dotfill
      \item[]<2> \begin{center}\textit{\textasciitilde\ Entrée\ \textasciitilde}\end{center}
      \item[]<2> \begin{center}Salade au saumon ou daurade (selon marché)\end{center}
      \item[]<3> \begin{center}\textit{\textasciitilde\ Plat\ \textasciitilde}\end{center}
      \item[]<3> \begin{center}Spaghetti ou Gnocchi (au choix)\end{center}
      \item[]<4> \begin{center}\textit{\textasciitilde\ Dessert\ \textasciitilde}\end{center}
      \item[]<4> \begin{center}Bananes (à volonté!)\end{center}
    \end{enumerate}
    \begin{center}
      \only<1>{\qquad avec 20F, je n'achète qu'un seul repas!}
      \only<2>{\qquad «selon marché»: un seul des deux, mais on me l'impose!}
      \only<3>{\qquad «au choix»: un seul des deux, mais \emph{je} choisis!}
      \only<4>{\qquad «à volonté!»: vérité pérenne, immuable, éternelle!}
    \end{center}
  \end{frame}

  \begin{frame}
    \frametitle{Reconstruction - Niveau -3}
    \framesubtitle{Retourner la table}
    \begin{itemize}
      \item La négation linéaire $\sim$ échange les points de vue
      \begin{itemize}
        \item<1->[] si A signifie "je consomme A" \\
                 (point du vue par exemple du client)
        \item<2->[] alors $\sim$ A signifie "je fournis A"\\
                 (point du vue du serveur/restaurant)
      \end{itemize}
    \end{itemize}
  \end{frame}

  \begin{frame}
    \frametitle{Reconstruction - Niveau -3}
    \framesubtitle{Vue des cuisines - dans le frigo}
    \setbeamercovered{transparent}
    \begin{enumerate}
      \item[]<1> \begin{center}Salade, saumon ou daurade\end{center}
      \item[]<2> \begin{center}Spaghetti et Gnocchi\end{center}
      \item[]<3> \begin{center}Bananes (beaucoup)\end{center}
      \item[] \dotfill
      \item[]<4> \begin{center}\textbf{20 Francs dans la caisse}\end{center}
    \end{enumerate}
    \begin{center}
      \only<1>{\qquad en fonction de ce que j'ai trouvé ce matin}
      \only<2>{\qquad je dois être prêt à servir les deux!}
      \only<3>{\qquad autant qu'on m'en demandera}
      \only<4>{\qquad en dépensant des ingrédients, j'ai gagné des sous}
    \end{center}
  \end{frame}

  \begin{frame}
    \frametitle{Reconstruction - Niveau -3}
    \framesubtitle{Et, ou, non, etc.}
    \setbeamercovered{transparent}
    \begin{itemize}
      \item<1-> Quand on prend le point de vue de ressources et d'échanges
      \begin{itemize}
      \item<2> "et" n'est pas tout à fait "et" (ça dépend si on offre ou reçoit)
      \item<3> "ou" n'est pas tout à fait "ou" (ça dépend si on offre ou reçoit)
      \item<4> "non" ne veut pas carrément pas dire "non", \\ mais "t'as qu'à le faire!"
      \end{itemize}
      \item<5> Girard a réintroduit le sujet ("je", "tu") dans la logique!
    \end{itemize}
    \pause
    
  \end{frame}

  \begin{frame}
    \frametitle{Reconstruction - Niveau -3}
    \framesubtitle{Types de session / protocoles}
    \begin{itemize}
      \item Permet de définir des types de session
      \pause
      \item[] \begin{center}\includegraphics[scale=1.5]{resources/session-types.png}\end{center}
      \pause 
      \item Un type de session correspond à un protocole client/serveur, 
            vu par une des parties
      \pause 
      \item Utiliser la négation linéaire permet d'avoir automatiquement
            le protocole vu par l'autre partie
    \end{itemize}
  \end{frame}

  \begin{frame}
    \frametitle{Merci!}
    \framesubtitle{Messages clés}
    \begin{itemize}
    \item informaticien, si tu ne t'intéresses pas à la logique,\newline 
          la logique s'intéresse à toi
    \item le c\oe ur de la logique est dans l'intéraction
    \item la \textbf{Vérité}? méta-bof... 
        \\ mais le dialogue? bien sûr! pourquoi pas?
    \end{itemize}
  \end{frame}

\end{document}